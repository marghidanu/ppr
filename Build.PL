#!/usr/bin/env perl

use strict;
use warnings;

use Module::Build;

my $builder = Module::Build->new(
	module_name => 'PPR',

	dist_author => 'Tudor Marghidanu',
	dist_abstract => 'Personal Perl Repo',

	share_dir => 'share',

	# NOTE: Keep list sorted!
	requires => {
		'CPAN::Repository' => undef,
		'experimental' => undef,
		'Mojolicious::Plugin::Directory' => undef,
		'Mojolicious::Plugin::Prometheus' => undef,
		'Mojolicious' => undef,
		'Moose' => undef,
		'Test::More' => undef,
	},

	optional => { },
);

$builder->create_build_script();
