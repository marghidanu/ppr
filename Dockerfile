FROM ubuntu:16.04

ENV PPR_DIRECTORY="/opt/repo"

COPY PPR*.tar.gz .

RUN  apt-get update -qq \
&& apt-get install -y build-essential curl \
&& curl -sL http://cpanmin.us | perl - --quiet App::cpanminus \
&& cpanm --notest --quiet --metacpan PPR*.tar.gz \
&& useradd -ms /bin/bash ppr \
&& mkdir $PPR_DIRECTORY \
&& chown -R ppr:ppr $PPR_DIRECTORY

USER ppr

EXPOSE 8080
VOLUME /opt/repo

ENTRYPOINT [ "ppr.pl", "daemon", "-m", "production", "-l", "http://*:8080" ]
