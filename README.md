# PPR

[![pipeline status](https://gitlab.com/marghidanu/ppr/badges/master/pipeline.svg)](https://gitlab.com/marghidanu/ppr/commits/master)

## Description

Private Perl repository with a REST API for uploading distributions into it.

This is a **Mojolicious** application built on top of **CPAN::Repository**. The application provides **Prometheus** support on the **/metrics** endpoint.

## Getting started

### Development environment

```bash
vagrant up
vagrant ssh

sudo su -
cd /vagrant

perl Build.PL
./Build installdeps
./Build
./Build test
```

### Running the server

```bash
PPR_DIRECTORY=/opt/repo ppr.pl daemon
```

#### Authorization

There is support for a token authorization but is pretty simple for now. The app allows you to set a secret via **PPR_SECRET** which has to be set back on each request via the **Authorization** header. If the environment variable is not set the **Authorization** header can be omitted.

```bash
PPR_SECRET=MySuperSecret PPR_DIRECTORY=/opt/repo ppr.pl daemon
```

### Docker

Well with **Docker** things get much easier:

```bash
docker pull registry.gitlab.com/marghidanu/ppr:latest
docker run -ti -p 3000:3000 \
	-v /opt/repo:/opt/repo \
	-e PPR_SECRET=secret \
	registry.gitlab.com/marghidanu/ppr:latest
```

also interacting with it should be a breeze:

```bash
curl -sl -X POST \
	-F "author=AUTHOR" \
	-F "distro=@Some-v2.0.0.tar.gz" \
	-F "distro=@Sample-v1.0.0.tar.gz" \
	-H "Authorization: secret" \
	http://localhost:3000/api/v1/distros
```

The example above show how to post multiple files at the same time, well you can do more if you want but keep in mind the the maximum request size cannot exceed 1GB.
