#!/usr/bin/env perl

package main {
	use Mojolicious::Commands;

	Mojolicious::Commands->start_app( 'PPR' );
}

__END__
