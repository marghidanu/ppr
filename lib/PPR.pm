package PPR {
	use Moose;

	use experimental qw( signatures );

	extends 'Mojolicious';

	our $VERSION = '0.0.0';

	use constant {
		DEFAULT_REQ_SIZE => 1073741824
	};

	sub startup( $self ) {
		# --- Configuration
		push( @{ $self->plugins()->namespaces() }, 'PPR::Plugin' );

		$self->max_request_size( DEFAULT_REQ_SIZE );

		# --- Plugins
		my $directory = $ENV{PPR_DIRECTORY};
		die( 'No repository path defined' )
			unless( $directory );

		$self->plugin( 'Directory',
			{
				root => $directory,
				auto_index => 0,
			}
		);

		$self->plugin( 'Repository',
			{
				directory => $directory,
				url => $ENV{PPR_URL},
			}
		);

		$self->plugin( 'Prometheus' );

		# --- Routes
		my $router = $self->routes();
		$router->any( '/' )->to( 'Default#index' );

		my $api = $router->under( '/api/v1' )
			->to( 'API#authorize' );

		$api->get( '/authors' )->to( 'API#authors' );
		$api->get( '/distros' )->to( 'API#modules' );
		$api->post( '/distros' )->to( 'API#upload' );
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__
