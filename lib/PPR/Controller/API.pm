package PPR::Controller::API {
	use Moose;

	use experimental qw( signatures );

	extends 'Mojolicious::Controller';

	use Mojo::File qw( tempdir );

	sub authorize( $self ) {
		return 1
			unless( exists( $ENV{PPR_SECRET} ) );

		# TODO: Review validation here ...

		my $authorization = $self->req()
			->headers()
			->authorization() || '';

		$self->render( json => {}, status => 401 ) && return undef
			if( $authorization ne $ENV{PPR_SECRET} );

		return 1;
	}

	sub modules( $self ) {
		my $modules = $self->repository()
			->packages()
			->modules();

		return $self->render( json => $modules );
	}

	sub authors( $self ) {
		my $aliases = $self->repository()
			->mailrc()
			->aliases();

		return $self->render( json => $aliases );
	}

	sub upload( $self ) {
		return $self->reply()->exception( 'File is too big' )
			if( $self->req()->is_limit_exceeded() );

		# Picking up the provided parameters
		my $directory = tempdir();
		my $author = $self->param( 'author' ) || 'PPR';

		# Iterating of the files in the request and adding them into the repository
		my @results = ();
		my $distros = $self->req()->every_upload( 'distro' );
		foreach my $distro ( @{ $distros } ) {
			my $path = $directory->child( $distro->filename() )
				->to_string();

			$distro->move_to( $path );
			$self->repository()
				->add_author_distribution( $author, $path );

			push( @results, $distro->filename() );
		}

		# Returning a list of files imported into the repository
		return $self->render( json => \@results );
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__
