package PPR::Controller::Default {
	use Moose;

	use experimental qw( signatures );

	extends 'Mojolicious::Controller';

	use PPR;

	sub index( $self ) {
		return $self->render( json => {
				name => 'PPR',
				version => $PPR::VERSION,
			}
		);
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__
