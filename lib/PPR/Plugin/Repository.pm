package PPR::Plugin::Repository {
	use Moose;

	use experimental qw( signatures );

	extends 'Mojolicious::Plugin';

	use CPAN::Repository;

	sub register( $self, $app, $config ) {
		$app->attr( repository => sub {
				return CPAN::Repository->new(
					dir => $config->{directory},
					written_by => $config->{author} || 'PPR',
				);
			}
		);

		$app->helper( repository => sub( $self ) {
				return $self->app()->repository();
			}
		);

		$app->hook( 'before_server_start' => sub( $server, $app ) {
				$app->repository()->initialize()
					unless( $app->repository()->is_initialized() );
			}
		);
	}

	__PACKAGE__->meta()->make_immutable( inline_constructor => 0 );
}

1;

__END__
