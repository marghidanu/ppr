package PPR::Utils {
	use Moose;
	use Moose::Exporter;

	use experimental qw( signatures );

	use File::ShareDir;

	use constant {
		DIST => 'PPR'
	};

	Moose::Exporter->setup_import_methods(
		as_is => [ qw( dist_file dist_dir ) ]
	);

	sub dist_file( $file ) {
		return File::ShareDir::dist_file( DIST, $file )
	}

	sub dist_dir() {
		return File::ShareDir::dist_dir( DIST )
	}

	__PACKAGE__->meta()->make_immutable();
}

1;

__END__
