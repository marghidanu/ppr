#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Mojo;

use Mojo::File qw( tempdir );

$ENV{PPR_DIRECTORY} = tempdir();

my $t = Test::Mojo->new( 'PPR' );

{
	$t->get_ok( '/' )
		->status_is( 200 )
		->content_type_like( qr/application\/json/ )
		->json_has( '/name' )
		->json_has( '/version' );
}

# --- Metrics
{

	$t->get_ok( '/metrics' )
		->status_is( 200 );
}

# --- Checking repository files
{
	$t->get_ok( '/modules/02STAMP' )
		->content_type_like( qr/text\/plain/ )
		->status_is( 200 );

	$t->get_ok( '/authors/01mailrc.txt.gz' )
		->content_type_like( qr/application\/x\-gzip/ )
		->status_is( 200 );

	$t->get_ok( '/authors/01mailrc.txt' )
		->content_type_like( qr/text\/plain/ )
		->status_is( 200 );

	$t->get_ok( '/modules/02packages.details.txt.gz' )
		->content_type_like( qr/application\/x\-gzip/ )
		->status_is( 200 );

	$t->get_ok( '/modules/02packages.details.txt' )
		->content_type_like( qr/text\/plain/ )
		->status_is( 200 );

	$t->get_ok( '/modules/06perms.txt.gz' )
		->content_type_like( qr/application\/x\-gzip/ )
		->status_is( 200 );

	$t->get_ok( '/modules/06perms.txt' )
		->content_type_like( qr/text\/plain/ )
		->status_is( 200 );
}

done_testing();
