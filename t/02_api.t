#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use Test::Mojo;

use Mojo::File qw( tempdir );

use PPR::Utils qw( dist_file );

# --- Configuration
$ENV{PPR_DIRECTORY} = tempdir();
$ENV{PPR_SECRET} = int( rand( 1000 ) );

my $t = Test::Mojo->new( 'PPR' );

my $headers = {
	Authorization => $ENV{PPR_SECRET},
};

# --- Distros
{
	# Uploading file to the service
	my $data = {
		distro => {
			file => dist_file( 'Sample-v1.0.0.tar.gz' )
		},
		author => 'TEST'
	};

	$t->post_ok( '/api/v1/distros' => $headers, form => $data )
		->status_is( 200 )
		->content_type_like( qr/application\/json/ )
		->json_is( [ 'Sample-v1.0.0.tar.gz' ] );

	# No authentication
	$t->post_ok( '/api/v1/distros' )
		->status_is( 401 )
		->content_type_like( qr/application\/json/ );

	# Listing modules
	$t->get_ok( '/api/v1/distros' => $headers )
		->status_is( 200 )
		->content_type_like( qr/application\/json/ )
		->json_is( '/Sample', [ 'v1.0.0', 'T/TE/TEST/Sample-v1.0.0.tar.gz' ] );

	# No authentication
	$t->get_ok( '/api/v1/distros' )
		->status_is( 401 )
		->content_type_like( qr/application\/json/ );
}

# --- Authors
{
	$t->get_ok( '/api/v1/authors' => $headers )
		->status_is( 200 )
		->content_type_like( qr/application\/json/ )
		->json_is( '/TEST', undef );

	# No authentication
	$t->get_ok( '/api/v1/authors' )
		->status_is( 401 )
		->content_type_like( qr/application\/json/ );
}

done_testing();
